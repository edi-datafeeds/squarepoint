EDI_MPAY_20160902
TableName	Actflag	AnnounceDate	Acttime	EventType	EventID	OptionID	SerialID	ResSectyCD	ResSecID	ResISIN	ResIssuername	ResSecurityDesc	RatioNew	RatioOld	Fractions	MinOfrQty	MaxOfrQty	MinQlyQty	MaxQlyQty	Paydate	CurenCD	MinPrice	MaxPrice	TndrStrkPrice	TndrStrkStep	Paytype	DutchAuction	DefaultOpt	OptElectionDate	OedExpTime	OedExpTimeZone	WrtExpTime	WrtExpTimeZone
MPAY	I	2001/10/12	2001/10/12	LIQ	49	1	1																				F	F					
MPAY	I	2016/06/03	2016/06/03	LIQ	1598	1	1													2016/06/16	USD					C	F	F					
MPAY	U	2007/03/24	2007/04/10	LIQ	784	1	1													2007/05/10	USD	1.65330	1.65330			C	F	F					
MPAY	I	2009/01/24	2009/01/24	LIQ	936	1	1														USD	5.93000	5.93000			C	F	F					
MPAY	C	2016/01/04	2016/01/05	LIQ	1560	1	1														USD					C	F	F					
MPAY	I	2010/03/01	2010/03/01	LIQ	1103	1	1	EQS					200000.0000000	1.0000000						2010/02/26						S	F	F					
MPAY	I	2010/03/01	2010/03/01	LIQ	1103	1	2	WAR												2010/02/26						S	F	F					
MPAY	I	2012/03/21	2012/03/21	LIQ	1257	1	1													2012/04/03	USD	0.02800	0.02800			C	F	F					
MPAY	I	2013/01/29	2013/01/29	LIQ	1333	1	1													2013/02/22	USD	5.00000	5.00000			C	F	F					
MPAY	I	2013/10/07	2013/10/07	LIQ	1333	1	2													2013/10/30	USD	1.50000	1.50000			C	F	F					
MPAY	I	2014/10/24	2014/10/24	LIQ	1333	1	3													2014/11/14	CAD	1.25000	1.25000			C	F	F					
MPAY	I	2014/08/20	2014/08/20	LIQ	1451	1	1	EQS	3677231	US92936P1003	WMIH Corp	Ordinary Shares														S	F	F					
MPAY	I	2014/09/20	2014/09/20	LIQ	1461	1	1														USD	0.05000	0.06000			C	F	F					
MPAY	I	2015/10/26	2015/10/26	LIQ	1538	1	1	WAR					0.0574298	1.0000000												S	F	F					
MPAY	I	2016/01/16	2016/01/18	LIQ	1538	1	2	WAR					0.0434284	1.0000000						2015/12/16						S	F	F					
MPAY	U	2015/08/04	2015/09/18	LIQ	1521	1	1													2015/09/21	USD	14.38000	14.38000			C	F	F					
EDI_ENDOFFILE
