EDI_IRCHG_20180613
TableName	Actflag	Created	Changed	IrchgID	SecID	ISIN	EffectiveDate	OldInterestRate	NewInterestRate	Eventtype	Notes
IRCHG	I	2013/04/22	2013/04/22	161081	170738	US35177PAL13	2002/02/12	9.50000	8.50000	CLEAN	
IRCHG	I	2016/02/01	2016/02/01	223183	170738	US35177PAL13	2013/09/01	8.50000	8.75000	CORR	As per EDI 160128(US35177PAL13).eml<BR><BR>Bond with a step-up clause (clause that triggers a change in interest payments if Orange s credit rating from the rating agencies changes). This clause was triggered in 2013 and in early 2014: the coupon due in March 2014 will be computed on the basis of an interest rate of 8.75% and after that date, the bond will bear interest at the rate of 9%.<BR>
IRCHG	I	2016/02/01	2016/02/01	223184	170738	US35177PAL13	2014/03/01	8.75000	9.00000	CORR	As per EDI 160128(US35177PAL13).eml<BR><BR>Bond with a step-up clause (clause that triggers a change in interest payments if Orange s credit rating from the rating agencies changes). This clause was triggered in 2013 and in early 2014: the coupon due in March 2014 will be computed on the basis of an interest rate of 8.75% and after that date, the bond will bear interest at the rate of 9%.<BR>
IRCHG	I	2018/03/23	2018/03/23	272606	5453785	XS1778929478	2018/03/07	6.375	6.125	CLEAN	
IRCHG	I	2018/06/13	2018/06/13	277826	5568763	US3134GSQF57	2019/06/28	2.85	3.05	IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277827	5568763	US3134GSQF57	2020/06/28	3.05	3.5	IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277828	5568763	US3134GSQF57	2021/06/28	3.5	3.75	IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277829	5568763	US3134GSQF57	2022/06/28	3.75	4	IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277832	5571469	US87161C6003	2023/06/21	6.3		IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277832	5571469	US87161C6003	2023/06/21	6.3		IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277833	5571515	US404280BT50	2028/06/19	4.583		IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277833	5571515	US404280BT50	2028/06/19	4.583		IRCHG	
IRCHG	I	2018/06/13	2018/06/13	277834	5568870	US46647PAS56	2021/06/18	3.514		IRCHG	
EDI_ENDOFFILE
